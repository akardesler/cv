![Photo](http://www.freelancem.com/file/3051/22.png)
# Haydar Kardesler
---
+48 608 453 384
hkardesler1@gmail.com
Jozefa Sowinskiego 3                      
Lublin, 20-400                          
Poland    
---
Education
---
2018-2022 (expected)
:   **BSc, Computer Science**; Wyższa Szkoła Przedsiębiorczości i Administracji w Lublinie

2014-2016
:   **Associate's Degree, Computer Programming**; Hitit University(Corum/Turkey)
---
Experience
---

- **Mobile Application Developer**
HK Apps, Freelance
Jan 2017 - Present

- **Mobile Application Developer**
[Fonet Bilgi Teknolojileri](https://www.fonetyazilim.com/language/en/homepage/)
Aug 2016 - Jan 2017

- **Android Developer**
Yon Medya
Apr 2014 - Jul 2017
---
Other
---

* **Programming Languages.**
    * Java, Swift, C#, SQL
    * Basic knowledge of C++, Pyhton, Javascript, PHP

* **Human Languages:**
     * Turkish (Native Speaker)
     * English (Professional Working Proficiency)
